import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  public mynotes: any = [];
  constructor(private route: Router, private storage: Storage, private alertCntrl: AlertController) {

  }

  ionViewDidLeave() {
    this.loadFunction();
  }
  ionViewDidEnter() {
    this.loadFunction();
  }
  ngOnInit() {
    this.loadFunction();
  }

  loadFunction() {
    this.storage.get('notes').then((response) => {
      let resp = [];
      let result = [];
      const currdate  = new Date();
      resp = response;
// tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < resp.length; i++) {
        const date = new Date(resp[i].date);
        if ( date => currdate ) {
          result.push(resp[i]);
        }
      }
      this.mynotes = result;
      console.log(this.mynotes);
    });
  }

  newNote() {
    this.route.navigateByUrl('/add-notes');
  }
  async delete( note ) {
    const alert = await this.alertCntrl.create
    ({
      header: 'Confirm Delete',
      message: 'Click Confirm to delete',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
      },
      {
        text: 'Okay',
        handler: () => {
          let notes = [];
          notes = this.mynotes;
          const index = notes.indexOf(note);
          notes.splice(index, 1);
          this.mynotes = notes;
          this.storage.set('notes', this.mynotes);
        }
      }
    ]
    });
    await alert.present();
  }
}
