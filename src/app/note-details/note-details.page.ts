import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.page.html',
  styleUrls: ['./note-details.page.scss'],
})
export class NoteDetailsPage implements OnInit {

  public note: any;
  public allNotes: any;
  public noteDetail: any;
  constructor(private activeroute: ActivatedRoute, private storage: Storage) { }

  ngOnInit() {
    this.note = this.activeroute.snapshot.paramMap.get('note');
    this.storage.get('notes').then((data) => {
      this.allNotes = data;
      this.noteDetail = this.findNoteDetails(this.allNotes, this.note);
      console.log(this.noteDetail);
    });
  }

  findNoteDetails(array, id) {
    const obj = array.find(obj => obj.id == id);
    return obj;
// tslint:disable-next-line: prefer-for-of
    // for ( let i = 0; i <= array.length; i++) {
    //   if ( array[i].id === id) {

    //     return array[i];

    //   }
    // }
  }

}
