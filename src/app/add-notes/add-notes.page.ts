import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { isArray } from 'util';
import { isNull } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-add-notes',
  templateUrl: './add-notes.page.html',
  styleUrls: ['./add-notes.page.scss'],
})
export class AddNotesPage implements OnInit {

  public allnotes: any = [];
  public date: any = new Date().toISOString();
  public mindate: any = new Date().toISOString().substr(0, 10);
  public time: any = '';
  public noteTitle: any = '';
  public newnotes: any = '';
  public newNote: {
    'dates': '',
    'time': '',
    'note': '',
  };

  public lastindex: any;
  constructor(private storage: Storage, private router: Router) {
    this.storage.get('notes').then((data) => {
      this.allnotes = data;
      let arr = [ ];
      arr = this.allnotes;
      let length = arr.length;
// tslint:disable-next-line: deprecation
      if ( typeof length === 'undefined' ) {
        this.lastindex = 1;
      } else {
        length += 1;
        this.lastindex = length;
      }
      console.log('NOTES & LAST INDEX', this.allnotes + ' ' + this.lastindex);
    });
  }

  ngOnInit() {
  }

  postNewNotes() {

    let date = new Date(this.date);
    let time = new Date(this.time);

    const request = {
      id: this.lastindex,
      date: date.toISOString().slice(0, 10),
      time: time.toLocaleTimeString(),
      note: this.newnotes,
      title: this.noteTitle
    };

    console.log(request);

    let noteslist = [];
    if (this.allnotes != null) {
      noteslist = this.allnotes;
    }

    noteslist.push(request);
    this.allnotes = noteslist;
    this.storage.set('notes', this.allnotes);
    this.router.navigateByUrl('/home');
  }

}
